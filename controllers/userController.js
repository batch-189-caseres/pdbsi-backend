const User = require('../models/User');
const Blacklist = require('../models/Blacklist');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const CryptoJS = require("crypto-js");


// Key for Encrypting And Decrypting
	const passphrase = `PDBSI-Official`;



module.exports.registerUser = (reqBody) => {

const date = new Date()
        	const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        	let time = date.getHours();
        	let newTime = ''
        	if (time > 12){
        		 newTime = `${time - 12}:${date.getMinutes()} pm`
        	} else if (time == 12) {
        		 newTime = `${time}:${date.getMinutes()} pm`
        	} else if (time == 0) {
        		 newTime = `${time + 12}:${date.getMinutes()} am`	
        	} else {
        		 newTime = `${time}:${date.getMinutes()} am`
        	}

const encrypted = CryptoJS.AES.encrypt(reqBody.email, passphrase).toString();

	let newUser = new User({
		email : encrypted,
		contacts: [
			{
				website : "PDBSI-Official",
				concern : reqBody.concern,
				ipAddress : reqBody.ipAddress,
				country : reqBody.country,
				city : reqBody.city,
			}
		],
		// dateCreated : `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()} ${newTime}`

	})

	return newUser.save().then((user, error) => {

// User registration failed
if(error) {
	return false

// User registration successful
} else {
	return {email: encrypted}
}

})
}


module.exports.decrypt = (reqBody) => {
	return User.find({email: reqBody.email}).then(user => {
	  const ciphertext = reqBody.email
      const bytes = CryptoJS.AES.decrypt(ciphertext, passphrase);
      const originalText = bytes.toString(CryptoJS.enc.Utf8);
      return originalText;
	 
	})
}

module.exports.countIpAddress = (reqBody) => {
	return User.find({ipAddress: reqBody.ipAddress}).then(user => {
		const count = user.length
		return `${count}`
	})
}


module.exports.blacklistEmail = (reqBody) => {
	let newBlacklist = new Blacklist({
		email : reqBody.email,
		reason : reqBody.reason
	})

	return newBlacklist.save().then((block, error) => {

	if(error) {
		return false

	} else {
		return true
	}
 })
}

module.exports.checkBlackListed = (reqBody) => {
	return Blacklist.find({email: reqBody.email}).then(user => {
		if(user.length === 1){
			return true
		} else {
			return false
		}
	})
}

module.exports.checkUser = (reqBody) => {
		return User.find({email: reqBody.email}).then(user => {
			if(user.length !== 0){
				if( user[0].email === reqBody.email){
					return true
				} else {
					return false
				}
			} else {
				return false
			}
	})
}

module.exports.addMessage = (reqBody) => {
	return User.find({email: reqBody.email}).then(user => {
		if(user.length !== 0){

			if( user[0].email === reqBody.email){
	            user[0].contacts.push({
	            	website: "PDBSI-Official", 
	            	concern: reqBody.concern, 
	            	country: reqBody.country, 
	            	city: reqBody.city, 
	            	ipAddress: reqBody.ipAddress, 
	            })
				        return user[0].save().then((newUser, error) => {
				           if(error) {
				                return false
				            } else {
				                return true
				            } 
				        })	
			} else {
				return false
			}
		
		} else {
			return false
		}
	})
}
