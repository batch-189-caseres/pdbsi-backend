const mongoose = require("mongoose");

const blacklistSchema = new mongoose.Schema({
	email : {
		type : String,
		required : [true, "email is required"] 
	},
	reason : {
		type: String, 
		required : [true, "reason is required"]
	},
	date : {
		type: Date,
		default : new Date()
	}
})


module.exports = mongoose.model("Blacklist", blacklistSchema);
